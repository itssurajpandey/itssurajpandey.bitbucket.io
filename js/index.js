var hamburger = document.querySelector(".hamburger");
var navMenu = document.querySelector(".nav-menu");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
  console.log("1");
  hamburger.classList.toggle("active");
  navMenu.classList.toggle("active");
}
var navLink = document.querySelectorAll(".nav-link");

navLink.forEach((n) => n.addEventListener("click", closeMenu));

function closeMenu() {
  console.log("2");
  hamburger.classList.remove("active");
  navMenu.classList.remove("active");
}


$(".banner-slider").slick({
  slidesToShow: 1,

  slidesToScroll: 1,

  dots: true,
  arrows: true,
});

$(".slider-section").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  dots: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1 } },
  ],
});
